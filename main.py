import pycom
import time
import machine
from network import WLAN
import socket
from wifi_link_ctl import WiFiLinkCtl
from lora_link_ctl import LoRaLinkCtl
from ble_ctl import BLEInterface

pycom.heartbeat(True)

throughput = 0


def handle_wifi_pkt(pkt, len, mac):
    print('lora pkt received from ' + str(mac))

def handle_wifi_evt(evt):
    print('wifi evt: ' + str(evt))

def handle_lora_pkt(pkt, len, mac):
    print('lora pkt received from ' + str(mac))

def handle_lora_evt(evt):
    print('lora evt: ' + str(evt))

wifi = WiFiLinkCtl(True, [0xAA, 0xBB], 8, 'abcd1234', 100)
lora = LoRaLinkCtl(True, [0xAA, 0xBB], 916800000, 9, 100)

wifi.reg_upper_cb(handle_wifi_pkt)
wifi.reg_evt_cb(handle_wifi_evt)

lora.reg_upper_cb(handle_lora_pkt)
lora.reg_evt_cb(handle_lora_evt)


ble_int = BLEInterface(wifi.enable, wifi.disable)

while True:
    time.sleep(0.5)
    

