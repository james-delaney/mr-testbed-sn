from wifi_link_ctl import WiFiLinkCtl
from lora_link_ctl import LoRaLinkCtl
from ble_ctl import BLEInterface
import time
import io
import _thread

class ServerCtl():

    def __init__(self):
        self.wifi = WiFiLinkCtl(True, [0xAA, 0xBB], 8, 'abcd1234', 100)
        self.lora = LoRaLinkCtl(True, [0xAA, 0xBB], 916800000, 9, 100)

        self.wifi.reg_upper_cb(self.handle_wifi_pkt)
        self.wifi.reg_evt_cb(self.handle_wifi_evt)

        self.lora.reg_upper_cb(self.handle_lora_pkt)
        self.lora.reg_evt_cb(self.handle_lora_evt)


        self.ble_int = BLEInterface(self.wifi.enable, self.wifi.disable)

        print('starting server_ctl thread')
        _thread.start_new_thread(self._server_ctl_thread, ())
        print('started server_ctl thread')

    def _server_ctl_thread(self):
        while True:
            
            wifi_link = self.wifi.get_en_status()
            lora_link = self.lora.get_init_status()

            #io.update_display(wifi_link, lora_link)

            time.sleep(0.5)
    
        return 0

    def handle_wifi_pkt(self, pkt, len, mac):
        print('lora pkt received from ' + str(mac))

    def handle_wifi_evt(self, evt):
        print('wifi evt: ' + str(evt))

    def handle_lora_pkt(self, pkt, len, mac):
        print('lora pkt received from ' + str(mac))

    def handle_lora_evt(self, evt):
        print('lora evt: ' + str(evt))

    