import machine 
#from ulab import vector

def _addr_to_bytes(addr):
        addr_str = addr.split('.')
        return bytes([int(addr_str[0]), int(addr_str[1]), int(addr_str[2]), int(addr_str[3])])

def _16bit_to_bytes(num):
    return bytes([((num & 0xFF00) >> 8), (num & 0x00FF)])

def _4_bytes_to_str(addr):
    return str(addr[0]) + '.' + str(addr[1]) + '.' + str(addr[2]) + '.' + str(addr[3])

def _16bit_2bytes_to_int(num):
    return ((num[0] << 8) + num[1])

'''
def randrange(start, stop=None):
    rang = stop-start
    result = int(vector.floor((machine.rng()/100000000) * (rang + 1) + start))
    return result
'''
'''
if stop is None:
    stop = start
    start = 0
upper = stop - start
bits = 0
pwr2 = 1
while upper > pwr2:
    pwr2 <<= 1
    bits += 1
while True:
    r = machine.rng()
    if r < upper:
        break
return r + start
'''
