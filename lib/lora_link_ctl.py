
import pycom
import time
import machine
from network import LoRa
import socket
import _thread
from machine import Timer

LINK_ACK_TIMEOUT = 3

class LoRaPkt():
    HEADER                      = 0     # 1 byte
    PKT_ID                      = 1     # 1 byte
    FROM_MAC_ADDR               = 2     # 2 bytes
    TO_MAC_ADDR                 = 4     # 2 bytes
    LEN                         = 6     # 1 bytes
    DATA                        = 7     # LEN bytes


class LoRaEvt():
    LORA_EVT_ANNOUNCE           = 0
    LORA_EVT_ANNOUNCE_ACK       = 1
    LORA_EVT_LINK_UP            = 2
    LORA_EVT_LINK_DN            = 3


LORA_HEADER                     = 0xBB

LORA_HEADER_SIZE                = 1
LORA_PKT_ID_SIZE                = 1
LORA_FROM_MAC_ADDR_SIZE         = 2
LORA_TO_MAC_ADDR_SIZE           = 2
LORA_LEN_SIZE                   = 1


LORA_PKT_ID_NORMAL              = 0
LORA_PKT_ID_ANNOUNCE            = 1
LORA_PKT_ID_ANNOUNCE_ACK        = 2


class LoRaLinkCtl():

    def __init__(self, srv, mac_addr, freq, sf, mtu):
        self.init_status = False
        self.mac_addr = bytes(mac_addr)
        self.freq = freq
        self.sf = sf
        self.mtu = mtu

        self.upper_pkt_cb = None
        self.new_client_cb = None
        self.client_down_cb = None
        self.evt_cb = None

        self.lora_peers = []

        self.lora_send_pkt_lock = _thread.allocate_lock()
        self.lora_send_pkt_buf = []
        self.lora_send_pkt_buf_size = 0

        self.mode = srv

        # link maintenance variables
        self.link_status_lock = _thread.allocate_lock()
        self.link_status = 0            # 0 = link down, 1 = link up
        self.link_ack_timeouts = 0      # number of announce ack periods missed
        self.ack_sent = 0
        self.ack_received = 0

        self.lora = LoRa(mode=LoRa.LORA, region=LoRa.AU915, tx_power=20, frequency=self.freq, bandwidth=LoRa.BW_125KHZ, sf=self.sf, power_mode=LoRa.ALWAYS_ON)
        self.lora_sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        self.lora_sock.setblocking(False)

        if (self.mode):
            # srv specific init code here
            pass
        else:
            # client specific init code here
            pass

        _thread.start_new_thread(self._lora_link_ctl_thread, ())
        
        if (not self.mode):
            self.lora_link_maint = Timer.Alarm(self._lora_link_maint_tick, LINK_ACK_TIMEOUT, periodic=True)

    def _lora_link_ctl_thread(self):
        self.init_status = True
        while True:
            data = self.lora_sock.recv(self.mtu)
            data_len = len(data)
            if (data_len > 0):
                if (data[LoRaPkt.HEADER] == LORA_HEADER):
                    if (data_len == int(data[LoRaPkt.LEN]+LoRaPkt.DATA)):
                        from_mac_addr = data[LoRaPkt.FROM_MAC_ADDR:LoRaPkt.FROM_MAC_ADDR+LORA_FROM_MAC_ADDR_SIZE]
                        to_mac_addr = data[LoRaPkt.TO_MAC_ADDR:LoRaPkt.TO_MAC_ADDR+LORA_TO_MAC_ADDR_SIZE]

                        pkt_type = data[LoRaPkt.PKT_ID]
                        #print('lora pkt id: ' + str(pkt_type))
                        if (pkt_type == LORA_PKT_ID_NORMAL):
                            upper_data_len = data[LoRaPkt.LEN]
                            self.upper_pkt_cb(data[LoRaPkt.DATA: LoRaPkt.DATA+upper_data_len])
                        elif (pkt_type == LORA_PKT_ID_ANNOUNCE):
                            self._peer_update(from_mac_addr)
                            self._send_announce_ack_pkt()
                            self.evt_cb(LoRaEvt.LORA_EVT_ANNOUNCE)
                        elif (pkt_type == LORA_PKT_ID_ANNOUNCE_ACK):
                            self._peer_update(from_mac_addr)
                            self.evt_cb(LoRaEvt.LORA_EVT_ANNOUNCE_ACK)

                            self.link_status_lock.acquire()
                            self.ack_received = 1
                            self.link_status_lock.release()
                        else:
                            # discard pkt
                            pass
                    else:
                        #discard pkt is length does not match
                        pass
                else:
                    # discard pkt if header does not match
                    pass

            if (self._pkt_to_send()):
                #print('sending lora pkt')
                pkt = self._get_next_pkt()
                send_pkt = self._build_pkt(pkt[0], pkt[1], pkt[2], pkt[3])
                self.lora_sock.send(send_pkt)


    def _lora_link_maint_tick(self, alarm):
        self.link_status_lock.acquire()
        if (self.ack_sent):
            if (self.ack_received):
                self.ack_sent = 0
                self.ack_received = 0
                self.link_ack_timeouts = 0

                self.link_status = 1
                self.evt_cb(LoRaEvt.LORA_EVT_LINK_UP)
            else:
                self.link_ack_timeouts += 1
                if (self.link_ack_timeouts > LINK_ACK_TIMEOUT):
                    self.link_status = 0
                    self.evt_cb(LoRaEvt.LORA_EVT_LINK_DN)
                    self._send_announce_pkt()
        else:
            self._send_announce_pkt()
            self.ack_sent = 1
        self.link_status_lock.release()

    def _peer_update(self, client_mac):
        pass

    def _get_next_pkt(self):
        pkt = None
        self.lora_send_pkt_lock.acquire()
        pkt = self.lora_send_pkt_buf.pop(self.lora_send_pkt_buf_size-1)
        self.lora_send_pkt_buf_size -= 1
        self.lora_send_pkt_lock.release()
        return pkt

    def _pkt_to_send(self):
        state = False
        self.lora_send_pkt_lock.acquire()
        if (self.lora_send_pkt_buf_size > 0):
            state = True
        self.lora_send_pkt_lock.release()

        return state

    def _build_pkt(self, data, data_len, pkt_type, dst_addr):
        pkt_data = bytearray(LoRaPkt.DATA+data_len)
        
        pkt_data[LoRaPkt.HEADER]                                                        = LORA_HEADER
        pkt_data[LoRaPkt.PKT_ID]                                                        = pkt_type
        pkt_data[LoRaPkt.FROM_MAC_ADDR:LoRaPkt.FROM_MAC_ADDR+LORA_FROM_MAC_ADDR_SIZE]   = self.mac_addr
        pkt_data[LoRaPkt.TO_MAC_ADDR:LoRaPkt.TO_MAC_ADDR+LORA_TO_MAC_ADDR_SIZE]        = dst_addr
        pkt_data[LoRaPkt.LEN]                                                           = data_len
        pkt_data[LoRaPkt.DATA:LoRaPkt.DATA+data_len]                                    = data
        return pkt_data

    def _send_announce_pkt(self):
        print('sending lora announce pkt')
        self.lora_send_pkt_lock.acquire()

        self.lora_send_pkt_buf.append([bytes([0]), 1, LORA_PKT_ID_ANNOUNCE, bytes([0xAA, 0xBB])])
        self.lora_send_pkt_buf_size += 1

        self.lora_send_pkt_lock.release()

    def _send_announce_ack_pkt(self):
        #print('sending lora announce ack pkt')
        self.lora_send_pkt_lock.acquire()

        rssi = self.lora.stats()[1]
        #self.lora_send_pkt_buf.append([bytes([0]), 1, LORA_PKT_ID_ANNOUNCE_ACK, bytes([0xAA, 0xBB])])
        self.lora_send_pkt_buf.append([bytearray([rssi]), 1, LORA_PKT_ID_ANNOUNCE_ACK, bytes([0xAA, 0xBB])])
        self.lora_send_pkt_buf_size += 1

        self.lora_send_pkt_lock.release()

    def reg_upper_cb(self, upper_cb):
        self.upper_pkt_cb = upper_cb

    def reg_new_client_cb(self, new_client_cb):
        self.new_client_cb = new_client_cb

    def reg_evt_cb(self, evt_cb):
        self.evt_cb = evt_cb

    def send_data(self, data, data_len, dst):
        self.lora_send_pkt_lock.acquire()

        self.lora_send_pkt_buf.append([data, data_len, LORA_PKT_ID_NORMAL, dst])
        self.lora_send_pkt_buf_size += 1

        self.lora_send_pkt_lock.release()    

    def get_init_status(self):
        return self.init_status

    def get_link_stats(self):
        if self.link_status:
            rssi = self.lora.stats()[1]
            return (self.link_status, rssi, 0)
        else:
            return (self.link_status, -200, 0)
