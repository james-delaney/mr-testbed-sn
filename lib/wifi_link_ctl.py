
import pycom
import time
import machine
from network import WLAN
import socket
import util as util
import _thread
from machine import Timer

LINK_ACK_TIMEOUT                    = 1
CLIENT_RECONNECT_TIMEOUT            = 1


class WiFiPkt():
    HEADER                          = 0      # 1 byte
    PKT_ID                          = 1      # 1 byte
    FROM_ADDR                       = 2      # 4 bytes
    FROM_PORT                       = 6      # 2 bytes
    MAC_ADDR                        = 8      # 2 bytes
    LEN                             = 10     # 1 byte
    DATA                            = 11     # LEN bytes


class WiFiEvt():
    WIFI_EVT_ANNOUNCE               = 0
    WIFI_EVT_ANNOUNCE_ACK           = 1
    WIFI_EVT_LINK_UP                = 2
    WIFI_EVT_LINK_DN                = 3

WIFI_HEADER                         = 0xBA

WIFI_HEADER_SIZE                    = 1
WIFI_PKT_ID_SIZE                    = 1
WIFI_FROM_ADDR_SIZE                 = 4
WIFI_FROM_PORT_SIZE                 = 2
WIFI_MAC_ADDR_SIZE                  = 2
WIFI_LEN_SIZE                       = 1

WIFI_PKT_ID_NORMAL                  = 0
WIFI_PKT_ID_ANNOUNCE                = 1
WIFI_PKT_ID_ANNOUNCE_ACK            = 2

class WiFiLinkCtl():

    def __init__(self, ap, mac_addr, chann, ap_pwd, mtu):
        self.init_status = False
        self.mac_addr = bytes(mac_addr)
        self.chann = chann
        self.ap_pwd = ap_pwd
        self.mtu = mtu

        self.upper_pkt_cb = None
        self.new_client_cb = None
        self.client_down_cb = None
        self.evt_cb = None

        self.wlan_peers = []

        self.wlan_send_pkt_lock = _thread.allocate_lock()
        self.wlan_send_pkt_buf = []
        self.wlan_send_pkt_buf_size = 0

        self.my_port = 11022
        self.my_port_bytes = util._16bit_to_bytes(self.my_port)

        self.ap_mode = ap
        self.wifi_en = True

        # link maintenance variables
        self.link_status_lock = _thread.allocate_lock()
        self.link_status = 0            # 0 = link down, 1 = link up
        self.link_ack_timeouts = 0      # number of announce ack periods missed
        self.ack_sent = 0
        self.ack_received = 0

        self.wifi_init()

        _thread.start_new_thread(self._wifi_link_ctl_thread, ())
        
        # we shouldn't start this unless we are actually connected to an ap - sending pkts without a conn is dumb
        if (not ap):
            self.wifi_link_maint = Timer.Alarm(self._wifi_link_maint_tick, LINK_ACK_TIMEOUT, periodic=True)

    def wifi_init(self):
        if (self.ap_mode):
            print('wifi in ap mode')
            self.wlan = WLAN(id=0, mode=WLAN.AP, ssid='MR_0', auth=(WLAN.WPA2, self.ap_pwd), channel=self.chann, antenna=WLAN.EXT_ANT)
            self.wlan.ifconfig(id=1, config=('10.0.0.1', '255.255.255.0', '10.0.0.1', '10.0.0.1'))
            self.wlan.max_tx_power(127)
            self.my_addr = '10.0.0.1'
            self.my_addr_bytes = bytes([10,0,0,1])
            self.my_port = 11022
            self.my_port_bytes = util._16bit_to_bytes(self.my_port)

            self.wlan_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.wlan_sock.bind(('10.0.0.1', 11022))
            self.wlan_sock.setblocking(False)
        else:
            self._wifi_client_connect()

        self.wifi_en = True

    def _wifi_client_connect(self):
        print('wifi client connecting')
        self.wlan = WLAN(mode=WLAN.STA)
        self.wlan.connect('MR_0', auth=(WLAN.WPA2, self.ap_pwd))
        if (not self.ap_mode):
            timeout = 0
            while (not timeout) and (not self.wlan.isconnected()): 
                time.sleep(CLIENT_RECONNECT_TIMEOUT)
                if (not self.wlan.isconnected()):
                    timeout = 1
                    #print('wifi client connect timeout')
            if (self.wlan.isconnected()):
                print('connected to ap')    
                print(self.wlan.ifconfig(id=0))
                self.my_addr = self.wlan.ifconfig(id=0)[0]
                self.my_addr_bytes = util._addr_to_bytes(self.my_addr)
                
                #self.my_port = 11022
                #self.my_port_bytes = util._16bit_to_bytes(self.my_port)

                self.wlan_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                self.wlan_sock.bind((self.wlan.ifconfig()[0], 11022))
                self.wlan_sock.setblocking(False)

        #if not self.ap_mode:
        #    self.wifi_link_maint = Timer.Alarm(self._wifi_link_maint_tick, LINK_ACK_TIMEOUT, periodic=True)

    def _wifi_link_ctl_thread(self):
        print('inside wifi link ctl thread')
        self.init_status = True
        while True:
            #print('wifi link ctl tick')
            #if (not self.ap_mode) and (self.wlan.isconnected()):
            if (self.wlan.isconnected()):
                data = self.wlan_sock.recv(self.mtu)
                data_len = len(data)
                if (data_len > 0):
                    if (data[WiFiPkt.HEADER] == WIFI_HEADER):
                        print('got some data')
                        #print(data_len)
                        #print(data[WiFiPkt.LEN])
                        if (data_len == int(data[WiFiPkt.LEN]+WiFiPkt.DATA)):
                            # check if this a new client
                            sender_mac_addr = data[WiFiPkt.MAC_ADDR: WiFiPkt.MAC_ADDR+WIFI_MAC_ADDR_SIZE]
                            sender_addr     = data[WiFiPkt.FROM_ADDR: WiFiPkt.FROM_ADDR+WIFI_FROM_ADDR_SIZE]
                            sender_port     = data[WiFiPkt.FROM_PORT:WiFiPkt.FROM_PORT+WIFI_FROM_PORT_SIZE]
                            '''
                            if (self._client_is_new(sender_mac_addr,
                                                    data[WiFiPkt.FROM_ADDR: WiFiPkt.FROM_ADDR+WIFI_FROM_ADDR_SIZE],
                                                    data[WiFiPkt.FROM_PORT:WiFiPkt.FROM_PORT+WIFI_FROM_PORT_SIZE])):
                                #self.new_client_cb(data[WiFiPkt.FROM_ADDR])
                            '''
                                # pass on only the data portion and its length to the upper server layer
                                
                            pkt_type = data[WiFiPkt.PKT_ID]
                            if (pkt_type == WIFI_PKT_ID_NORMAL):
                                #print('sending rx pkt to upper')
                                upper_data_len = data[WiFiPkt.LEN]
                                self.upper_pkt_cb(data[ WiFiPkt.DATA : WiFiPkt.DATA + upper_data_len ], upper_data_len, sender_mac_addr)
                            elif (pkt_type == WIFI_PKT_ID_ANNOUNCE):
                                self._peer_update(sender_mac_addr, 
                                                sender_addr,
                                                sender_port)
                                self._send_announce_ack_pkt(sender_addr, sender_port)
                                self.evt_cb(WiFiEvt.WIFI_EVT_ANNOUNCE)
                            elif (pkt_type == WIFI_PKT_ID_ANNOUNCE_ACK):
                                print('received announce ack')
                                self._peer_update(sender_mac_addr, 
                                                sender_addr,
                                                sender_port)
                                self.evt_cb(WiFiEvt.WIFI_EVT_ANNOUNCE_ACK)

                                self.link_status_lock.acquire()
                                self.ack_received = 1
                                self.link_status_lock.release()
                        else:
                            # discard if length of received data does not match length of the "packet"
                            pass
                    else:
                        # discard packet if header does not match
                        pass

                if (self._pkt_to_send()):
                    pkt = self._get_next_pkt()
                    #print(pkt)
                    send_pkt = self._build_pkt(pkt[0], pkt[1], pkt[2])
                    #print(send_pkt)
                    if (pkt[2] == WIFI_PKT_ID_NORMAL):
                        dst = self._get_send_ip_addr(pkt[3])
                    else:
                        dst = pkt[3]
                    self.wlan_sock.sendto(send_pkt, dst)
            elif not self.ap_mode:
                self._wifi_client_connect()     # try to reconnect and wait for timeout
            else:
                # wait for a client to connect
                #print('client not connected')
                pass
                
                
            
    def _wifi_link_maint_tick(self, alarm):
        print('wifi link maint tick')
        self.link_status_lock.acquire()
        if (self.ack_sent):
            if (self.ack_received):
                print('wifi announce ack rx')
                self.ack_sent = 0
                self.ack_received = 0
                self.link_ack_timeouts = 0

                self.link_status = 1
                #print('sending wifi evt link up')
                self.evt_cb(WiFiEvt.WIFI_EVT_LINK_UP)
            else:
                self.link_ack_timeouts += 1
                if (self.link_ack_timeouts > LINK_ACK_TIMEOUT):
                    print('wifi sending announce - ack timeout')
                    self.link_status = 0
                    #print('sending wifi evt link down')
                    self.evt_cb(WiFiEvt.WIFI_EVT_LINK_DN)
                    self._send_announce_pkt()
        else:
            #print('wifi sending announce')
            self._send_announce_pkt()
            self.ack_sent = 1
        self.link_status_lock.release()

    def _peer_update(self, client_mac, client_addr, client_port):
        for c in self.wlan_peers:
            if (c[0] == client_mac):
                return False

        ip_addr = str(int(client_addr[0]))+'.'+str(int(client_addr[1]))+'.'+str(int(client_addr[2]))+'.'+str(int(client_addr[3]))
        port = int(client_port[0] << 8) + int(client_port[1])

        self.wlan_peers.append((client_mac, ip_addr, port))
        return True

    def _get_next_pkt(self):
        pkt = None
        self.wlan_send_pkt_lock.acquire()
        pkt = self.wlan_send_pkt_buf.pop(self.wlan_send_pkt_buf_size-1)
        self.wlan_send_pkt_buf_size -= 1
        self.wlan_send_pkt_lock.release()
        return pkt

    def _pkt_to_send(self):
        state = False
        self.wlan_send_pkt_lock.acquire()
        if (self.wlan_send_pkt_buf_size > 0):
            state = True
        self.wlan_send_pkt_lock.release()

        return state

    def _build_pkt(self, data, data_len, pkt_type):
        pkt_data = bytearray(WiFiPkt.DATA+data_len)


        pkt_data[WiFiPkt.HEADER]                                            = WIFI_HEADER
        pkt_data[WiFiPkt.PKT_ID]                                            = pkt_type
        pkt_data[WiFiPkt.FROM_ADDR:WiFiPkt.FROM_ADDR+WIFI_FROM_ADDR_SIZE]   = self.my_addr_bytes
        pkt_data[WiFiPkt.FROM_PORT:WiFiPkt.FROM_PORT+WIFI_FROM_PORT_SIZE] = self.my_port_bytes
        pkt_data[WiFiPkt.MAC_ADDR:WiFiPkt.MAC_ADDR+WIFI_MAC_ADDR_SIZE] = self.mac_addr
        pkt_data[WiFiPkt.LEN]                                               = data_len
        pkt_data[WiFiPkt.DATA:WiFiPkt.DATA+data_len]                        = data
        return pkt_data

    

    def _get_send_ip_addr(self, mac):
        #print(self.wlan_peers)
        for m in self.wlan_peers:
            if (m[0][0] == mac[0]) and (m[0][1] == mac[1]):
                return (m[1], m[2])
        return None

    def _send_announce_pkt(self):
        
        gw_addr = self.wlan.ifconfig(id=0)[2]
        print('sending wifi announce pkt to: ' + gw_addr)
        self.wlan_send_pkt_lock.acquire()
        
        self.wlan_send_pkt_buf.append([bytes([0]), 1, WIFI_PKT_ID_ANNOUNCE, (gw_addr, self.my_port)])
        self.wlan_send_pkt_buf_size += 1

        self.wlan_send_pkt_lock.release()

    def _send_announce_ack_pkt(self, addr, port):
        print('sending wifi announce ack pkt')
        self.wlan_send_pkt_lock.acquire()
        
        rssi = self.wlan.ap_sta_list()[0][1]
        self.wlan_send_pkt_buf.append([bytearray([rssi]), 1, WIFI_PKT_ID_ANNOUNCE_ACK, (util._4_bytes_to_str(addr), util._16bit_2bytes_to_int(port))])
        self.wlan_send_pkt_buf_size += 1

        self.wlan_send_pkt_lock.release()

    def reg_upper_cb(self, upper_cb):
        self.upper_pkt_cb = upper_cb
    

    def reg_new_client_cb(self, new_client_cb):
        self.new_client_cb = new_client_cb

    def reg_evt_cb(self, evt_cb):
        self.evt_cb = evt_cb

    def send_data(self, data, data_len, dst):
        self.wlan_send_pkt_lock.acquire()
        
        self.wlan_send_pkt_buf.append([data, data_len, WIFI_PKT_ID_NORMAL, dst])
        self.wlan_send_pkt_buf_size += 1

        self.wlan_send_pkt_lock.release()
    
    def get_init_status(self):
        return self.init_status

    def get_en_status(self):
        return self.wifi_en

    def get_link_stats(self):
        if self.wlan.isconnected():
            rssi = self.wlan.joined_ap_info()[3]
            return (self.link_status, rssi, 0)
        else:
            #self.wifi_link_maint.cancel()
            return (self.link_status, -200, 0)

    def disable(self):
        self.wlan.deinit()
        self.wifi_en = False

    
    def enable(self):
        if not self.wifi_en:
            self.wifi_init()
        
