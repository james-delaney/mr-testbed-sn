from machine import UART
import _thread
import time

sample_log_msg = {  'timestamp':        4,
                    'wifi_link':        1,
                    'lora_link':        1,
                    'wifi_rssi':        2,
                    'lora_rssi':        2,
                    'active_radio':     1,
                    'lat':              9,
                    'lon':              9,
}

min_log_msg = (     ('timestamp',        4,),
                    ('wifi_link',        1,),
                    ('lora_link',        1,),
                    ('wifi_rssi',        2,),
                    ('lora_rssi',        2,),
                    ('active_radio',     1,),
)


class UARTLogMsg():

    def __init__(self, log_msg_struct):
        self.log_struct = log_msg_struct
        self.msg_len = 0

        for l in log_msg_struct:
            self.msg_len += l[1]
            #self.log_dict[l[0]] = l[1]


    def len(self):
        return self.msg_len

    def build_msg(self, **kwargs):
        msg_bytes = bytearray(b'\xff\xaa\xff')
        for d, d_len in self.log_struct:
            try:
                #msg_bytes.append(int(kwargs.pop(d)).to_bytes(d_len, 'big'))
                a = int(kwargs.pop(d)).to_bytes(d_len, 'big')
                #print(d, d_len, a)
                msg_bytes.extend(a)
            except KeyError:
                print('UARTLogMsg.build_msg got non-existent var: ', d)

        msg_bytes.extend(b'\xff\xaa\xff')
        return msg_bytes



class UARTLogInterface():

    def __init__(self, log_format, receiver=False, uart_iface=1, baudrate=115200):
        
        self.log_format = log_format
        self.uart = None
        self.uart_iface = uart_iface
        self.baudrate = baudrate

        self.rx_queue = []
        self.tx_queue = []
        self.rx_queue_lock = _thread.allocate_lock()
        self.tx_queue_lock = _thread.allocate_lock()

        self.uart = UART(self.uart_iface, baudrate=self.baudrate, parity=None, bits=8, stop=1)

        _thread.start_new_thread(self._logging_thread, ())

    def log_uart_msg(self, log_msg):
        
        self.tx_queue_lock.acquire()
        #print('queue uart log msg')
        #print(log_msg)
        self.tx_queue.append(log_msg)
        self.tx_queue_lock.release()


    def _logging_thread(self):
        
        while True:
            #print('logging rx thread tick')
            d = self.uart.readline()
            if d is not None:
                # block and wait unconditionally to get rx lock if we have data that might get lost
                self.rx_queue_lock.acquire(waitflag=1, timeout=-1)
                self.rx_queue.append(d)
                self.rx_queue_lock.release()

            self.rx_queue_lock.acquire()
            if len(self.rx_queue) > 0:
                d = self.rx_queue.pop(0)
                _thread.start_new_thread(self._proc_rx_msg, (d))
            self.rx_queue_lock.release()

            self.tx_queue_lock.acquire()
            #print('try tx uart log msg')
            if len(self.tx_queue) > 0:
                #print('sending uart log msg')
                self.uart.write(self.tx_queue.pop(0))
            self.tx_queue_lock.release()

            time.sleep(0.01)

    def _proc_rx_msg(self, data):
        pass

