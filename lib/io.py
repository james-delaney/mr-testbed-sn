
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C

i2c_m = None
ssd1306 = None
def init_io():
    global i2c_m

    i2c_m = I2C(0, I2C.MASTER, baudrate=100000, pins=("P9", "P10"))

    init_display()


def init_display():
    global i2c_m, ssd1306

    ssd1306 = SSD1306_I2C(128, 64, i2c_m)

    ssd1306.text("MR Server", 0, 0)
    ssd1306.show()

def update_display(wifi_status, lora_status):
    
    wifi_link = wifi_status
    #wifi_rssi = wifi_status[1]
    lora_link = lora_status
    #lora_rssi = lora_status[1]

    ssd1306.fill(0)

    ssd1306.text("MR Server", 0, 0)

    if wifi_link:
        ssd1306.text('WiFi: UP', 0, 17)
    else:
        ssd1306.text('WiFi: DN', 0, 16)

    if lora_link:
        ssd1306.text('LoRa: UP', 0, 25)
    else:
        ssd1306.text('LoRa: DN', 0, 25)

    ssd1306.show()

