from network import Bluetooth

# def conn_cb (bt_o):
#     events = bt_o.events()
#     if  events & Bluetooth.CLIENT_CONNECTED:
#         print("Client connected")
#     elif events & Bluetooth.CLIENT_DISCONNECTED:
#         print("Client disconnected")

# def char1_cb_handler(chr, data):

#     # The data is a tuple containing the triggering event and the value if the event is a WRITE event.
#     # We recommend fetching the event and value from the input parameter, and not via characteristic.event() and characteristic.value()
#     events, value = data
#     if  events & Bluetooth.CHAR_WRITE_EVENT:
#         print("Write request with value = {}".format(value))
#     else:
#         print('Read request on char 1')

# def char2_cb_handler(chr, data):
#     # The value is not used in this callback as the WRITE events are not processed.
#     events, value = data
#     if  events & Bluetooth.CHAR_READ_EVENT:
#         print('Read request on char 2')
#     if events & Bluetooth.CHAR_WRITE_EVENT:
#         print('Write request on char 2 with value = {}'.format(value))

# bluetooth = Bluetooth(modem_sleep=True)
# bluetooth.set_advertisement(name='FiPy', service_uuid=b'1234567890123456')
# bluetooth.callback(trigger=Bluetooth.CLIENT_CONNECTED | Bluetooth.CLIENT_DISCONNECTED, handler=conn_cb)
# bluetooth.advertise(True)

# srv1 = bluetooth.service(uuid=b'1234567890123456', isprimary=True)
# chr1 = srv1.characteristic(uuid=b'ab34567890123456', value=5)
# char1_cb = chr1.callback(trigger=Bluetooth.CHAR_WRITE_EVENT | Bluetooth.CHAR_READ_EVENT, handler=char1_cb_handler)

# srv2 = bluetooth.service(uuid=1234, isprimary=True)
# chr2 = srv2.characteristic(uuid=4567, value=0x1234)
# char2_cb = chr2.callback(trigger=Bluetooth.CHAR_WRITE_EVENT | Bluetooth.CHAR_READ_EVENT, handler=char2_cb_handler)

class BLEInterface():

    def __init__(self, wifi_toggle_on_cb, wifi_toggle_off_cb):
        bluetooth = Bluetooth(modem_sleep=True)
        bluetooth.set_advertisement(name='FiPy', service_uuid=b'1234567890123456')
        bluetooth.callback(trigger=Bluetooth.CLIENT_CONNECTED | Bluetooth.CLIENT_DISCONNECTED, handler=self.conn_cb)
        bluetooth.advertise(True)

        srv1 = bluetooth.service(uuid=0000, isprimary=True)
        chr1 = srv1.characteristic(uuid=1, value=0)
        chr1_cb = chr1.callback(trigger=Bluetooth.CHAR_WRITE_EVENT, handler=self.wifi_switch_handler)

        self.wifi_toggle_on = wifi_toggle_on_cb
        self.wifi_toggle_off = wifi_toggle_off_cb



    def conn_cb(self, bt_o):
        events = bt_o.events()
        if  events & Bluetooth.CLIENT_CONNECTED:
            print("Client connected")
        elif events & Bluetooth.CLIENT_DISCONNECTED:
            print("Client disconnected")

    def wifi_switch_handler(self, chr, data):
        events, value = data

        if events & Bluetooth.CHAR_WRITE_EVENT:
            #print('got write evt wifi swtich ', int(value[0]))
            if int(value[0]) == 49: # value = '1'
                print('turning wifi on')
                self.wifi_toggle_on()
            elif int(value[0]) == 48: # value = '0'
                print('turning wifi off')
                self.wifi_toggle_off()